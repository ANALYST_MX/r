library(ggplot2)

load("train.RData")
my_df <- train
    
str(my_df)
# 'data.frame':   150 obs. of  5 variables:
# $ gender: Factor w/ 2 levels "female","male": 2 2 2 2 2 2 2 2 2 2 ...
# $ read  : int  57 44 63 47 50 63 57 60 57 73 ...
# $ write : int  52 33 44 52 59 57 55 46 65 60 ...
# $ math  : int  41 54 47 57 42 54 52 51 51 71 ...
# $ hon   : Factor w/ 2 levels "N","Y": 1 1 1 1 1 1 1 1 2 1 ...    

ggplot(my_df, aes(read, math, col = gender)) +
       geom_point(size = 5) +
       facet_grid(.~hon) +
       theme(axis.text = element_text(size = 25),
             axis.title = element_text(size = 25,face = "bold"))

fit <- glm(hon ~ read + math + gender, my_df, family = "binomial")
# Call:  glm(formula = hon ~ read + math + gender, family = "binomial",
#     data = my_df)
#
# Coefficients:
# (Intercept)         read         math   gendermale
#   -12.12407      0.06677      0.13907     -1.18606
#
# Degrees of Freedom: 149 Total (i.e. Null);  146 Residual
# Null Deviance:      163
# Residual Deviance: 109.7        AIC: 117.7

summary(fit)
# Call:
# glm(formula = hon ~ read + math + gender, family = "binomial",
#     data = my_df)
#
# Deviance Residuals:
#     Min       1Q   Median       3Q      Max
#     -1.8173  -0.5989  -0.3086  -0.1087   2.3626
# 
# Coefficients:
#              Estimate Std. Error z value Pr(>|z|)
# (Intercept) -12.12407    2.14263  -5.659 1.53e-08 ***
# read          0.06677    0.03291   2.029  0.04247 *
# math          0.13907    0.04243   3.277  0.00105 **
# gendermale   -1.18606    0.51326  -2.311  0.02084 *
# ---
# Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
#
# (Dispersion parameter for binomial family taken to be 1)
#
#     Null deviance: 162.98  on 149  degrees of freedom
# Residual deviance: 109.75  on 146  degrees of freedom
# AIC: 117.75
#
# Number of Fisher Scoring iterations: 5

exp(fit$coefficients)
#  (Intercept)         read         math   gendermale
# 5.427272e-06 1.069055e+00 1.149203e+00 3.054224e-01
 
head(predict(object = fit))
#         1         2         3         4         5         6
# -3.802171 -2.862355 -2.567112 -2.244825 -4.130526 -1.593634
	
head(predict(object = fit, type = "response"))
#          1          2          3          4          5          6
# 0.02183486 0.05404619 0.07128525 0.09579675 0.01582013 0.16887324

my_df$prob <- predict(object = fit, type = "response")

str(my_df)
# 'data.frame':   150 obs. of  8 variables:
# $ gender   : Factor w/ 2 levels "female","male": 2 2 2 2 2 2 2 2 2 2 ...
# $ read     : int  57 44 63 47 50 63 57 60 57 73 ...
# $ write    : int  52 33 44 52 59 57 55 46 65 60 ...
# $ math     : int  41 54 47 57 42 54 52 51 51 71 ...
# $ hon      : Factor w/ 2 levels "N","Y": 1 1 1 1 1 1 1 1 2 1 ...
# $ prob     : num  0.0218 0.054 0.0713 0.0958 0.0158 ...

library(ROCR)

pred_fit <- prediction(my_df$prob, my_df$hon)

str(pred_fit)
# Formal class 'prediction' [package "ROCR"] with 11 slots
# ..@ predictions:List of 1
# .. ..$ : num [1:150] 0.0218 0.054 0.0713 0.0958 0.0158 ...
# ..@ labels     :List of 1
# .. ..$ : Ord.factor w/ 2 levels "N"<"Y": 1 1 1 1 1 1 1 1 2 1 ...
# ..@ cutoffs    :List of 1
# .. ..$ : num [1:140] Inf 0.933 0.876 0.845 0.843 ...
# ..@ fp         :List of 1
# .. ..$ : num [1:140] 0 0 0 0 0 0 0 1 1 2 ...
# ..@ tp         :List of 1
# .. ..$ : num [1:140] 0 1 2 4 5 6 7 8 9 9 ...
# ..@ tn         :List of 1
# .. ..$ : num [1:140] 115 115 115 115 115 115 115 114 114 113 ...
# ..@ fn         :List of 1
# .. ..$ : num [1:140] 35 34 33 31 30 29 28 27 26 26 ...
# ..@ n.pos      :List of 1
# .. ..$ : int 35
# ..@ n.neg      :List of 1
# .. ..$ : int 115
# ..@ n.pos.pred :List of 1
# .. ..$ : num [1:140] 0 1 2 4 5 6 7 9 10 11 ...
# ..@ n.neg.pred :List of 1
# .. ..$ : num [1:140] 150 149 148 146 145 144 143 141 140 139 ...
					    
perf_fit <- performance(pred_fit, "tpr", "fpr")

str(perf_fit)
# Formal class 'performance' [package "ROCR"] with 6 slots
# ..@ x.name      : chr "False positive rate"
# ..@ y.name      : chr "True positive rate"
# ..@ alpha.name  : chr "Cutoff"
# ..@ x.values    :List of 1
# .. ..$ : num [1:140] 0 0 0 0 0 ...
# ..@ y.values    :List of 1
# .. ..$ : num [1:140] 0 0.0286 0.0571 0.1143 0.1429 ...
# ..@ alpha.values:List of 1
# .. ..$ : num [1:140] Inf 0.933 0.876 0.845 0.843 ...
		  
plot(perf_fit, colorize=T , print.cutoffs.at = seq(0,1,by=0.1))

auc <- performance(pred_fit, measure = "auc")
# An object of class "performance"
# Slot "x.name":
# [1] "None"
#
# Slot "y.name":
# [1] "Area under the ROC curve"
#
# Slot "alpha.name":
# [1] "none"
#
# Slot "x.values":
# list()
#
# Slot "y.values":
# [[1]]
# [1] 0.8703106
#
#
# Slot "alpha.values":
# list()

str(auc)
# Formal class 'performance' [package "ROCR"] with 6 slots
# ..@ x.name      : chr "None"
# ..@ y.name      : chr "Area under the ROC curve"
# ..@ alpha.name  : chr "none"
# ..@ x.values    : list()
# ..@ y.values    :List of 1
# .. ..$ : num 0.87
# ..@ alpha.values: list()
	      
perf3 <- performance(pred_fit, x.measure = "cutoff", measure = "spec")
# Formal class 'performance' [package "ROCR"] with 6 slots
# ..@ x.name      : chr "Cutoff"
# ..@ y.name      : chr "Specificity"
# ..@ alpha.name  : chr "none"
# ..@ x.values    :List of 1
# .. ..$ : num [1:140] Inf 0.933 0.876 0.845 0.843 ...
# ..@ y.values    :List of 1
# .. ..$ : num [1:140] 1 1 1 1 1 ...
# ..@ alpha.values: list()
		
perf4 <- performance(pred_fit, x.measure = "cutoff", measure = "sens")
# Formal class 'performance' [package "ROCR"] with 6 slots
# ..@ x.name      : chr "Cutoff"
# ..@ y.name      : chr "Sensitivity"
# ..@ alpha.name  : chr "none"
# ..@ x.values    :List of 1
# .. ..$ : num [1:140] Inf 0.933 0.876 0.845 0.843 ...
# ..@ y.values    :List of 1
# .. ..$ : num [1:140] 0 0.0286 0.0571 0.1143 0.1429 ...
# ..@ alpha.values: list()
		
perf5 <- performance(pred_fit, x.measure = "cutoff", measure = "acc")
# Formal class 'performance' [package "ROCR"] with 6 slots
# ..@ x.name      : chr "Cutoff"
# ..@ y.name      : chr "Accuracy"
# ..@ alpha.name  : chr "none"
# ..@ x.values    :List of 1
# .. ..$ : num [1:140] Inf 0.933 0.876 0.845 0.843 ...
# ..@ y.values    :List of 1
# .. ..$ : num [1:140] 0.767 0.773 0.78 0.793 0.8 ...
# ..@ alpha.values: list()		

plot(perf3, col = "red", lwd =2)

plot(add = T, perf4 , col = "green", lwd =2)

plot(add = T, perf5, lwd =2)

legend(x = 0.6, y = 0.3, c("spec", "sens", "accur"),
       lty = 1, col = c('red', 'green', 'black'), bty = 'n', cex = 1, lwd = 2)

abline(v = 0.225, lwd = 2)

my_df$pred_resp <- factor(ifelse(my_df$prob > 0.225, 1, 0), labels = c("N", "Y"))

my_df$correct <- ifelse(my_df$pred_resp == my_df$hon, 1, 0)

str(my_df)
# 'data.frame':   150 obs. of  8 variables:
# $ gender   : Factor w/ 2 levels "female","male": 2 2 2 2 2 2 2 2 2 2 ...
# $ read     : int  57 44 63 47 50 63 57 60 57 73 ...
# $ write    : int  52 33 44 52 59 57 55 46 65 60 ...
# $ math     : int  41 54 47 57 42 54 52 51 51 71 ...
# $ hon      : Factor w/ 2 levels "N","Y": 1 1 1 1 1 1 1 1 2 1 ...
# $ prob     : num  0.0218 0.054 0.0713 0.0958 0.0158 ...
# $ pred_resp: Factor w/ 2 levels "N","Y": 1 1 1 1 1 1 1 1 1 2 ...
# $ correct  : num  1 1 1 1 1 1 1 1 0 0 ...

ggplot(my_df, aes(prob, fill = factor(correct))) +
       geom_dotplot() +
       theme(axis.text = element_text(size = 25),
             axis.title = element_text(size = 25,face = "bold"))

mean(my_df$correct)
# [1] 0.7666667

test_df <- binomial_regression_test

str(test_df)
# 'data.frame':   50 obs. of  5 variables:
# $ gender: Factor w/ 2 levels "female","male": 1 2 2 1 2 1 2 2 2 2 ...
# $ read  : int  68 34 73 50 42 47 63 47 63 44 ...
# $ write : int  59 46 62 67 49 44 49 47 65 44 ...
# $ math  : int  53 45 73 66 43 42 49 41 48 46 ...
# $ hon   : Factor w/ 2 levels "N","Y": 1 1 2 2 1 1 1 1 2 1 ...

test_df$hon <- NA

test_df$hon <- predict(fit, newdata = test_df, type = "response")

str(test_df)
# 'data.frame':   50 obs. of  5 variables:
# $ gender: Factor w/ 2 levels "female","male": 1 2 2 1 2 1 2 2 2 2 ...
# $ read  : int  68 34 73 50 42 47 63 47 63 44 ...
# $ write : int  59 46 62 67 49 44 49 47 65 44 ...
# $ math  : int  53 45 73 66 43 42 49 41 48 46 ...
# $ hon   : num  0.44701 0.00831 0.84767 0.59705 0.01071 ...
     
test_df
