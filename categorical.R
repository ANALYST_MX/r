load("grants.RData")
df <- grants

str(df)

df$status <- factor(df$status, labels = c("Not funded", "Funded"))
# df$status <- as.factor(df$status)
# levels(df$status) <- c("Not funded", "Funded")

t1 <- table(df$status)
# Not funded     Funded
#        747        673

dim(t1)
# [1] 2

t2 <- table(df$status, df$field)
#            beh_cog bio chem physics soc
# Not funded     100 473   60      70  44
# Funded          65 432   66      78  32

t2 <- table(status = df$status, field = df$field)
#             field
# status       beh_cog bio chem physics soc
#   Not funded     100 473   60      70  44
#   Funded          65 432   66      78  32

dim(t2)
# [1] 2 5

prop.table(t2)
#             field
# status          beh_cog        bio       chem    physics        soc
#   Not funded 0.07042254 0.33309859 0.04225352 0.04929577 0.03098592
#   Funded     0.04577465 0.30422535 0.04647887 0.05492958 0.02253521

prop.table(t2, 1)
#             field
# status          beh_cog        bio       chem    physics        soc
#   Not funded 0.13386881 0.63319946 0.08032129 0.09370817 0.05890228
#   Funded     0.09658247 0.64190193 0.09806835 0.11589896 0.04754829

prop.table(t2, 2)
#             field
# status         beh_cog       bio      chem   physics       soc
#   Not funded 0.6060606 0.5226519 0.4761905 0.4729730 0.5789474
#   Funded     0.3939394 0.4773481 0.5238095 0.5270270 0.4210526

t3 <- table(Years = df$years_in_uni, Field = df$field, Status = df$status)
# , , Status = Not funded
#
#       Field
# Years  beh_cog bio chem physics soc
#   < 5       57 198   31      20  22
#   > 10      29 144   28      47  16
#   5-10      14 131    1       3   6
#
# , , Status = Funded
#
#       Field
# Years  beh_cog bio chem physics soc
#   < 5       27 180   41      22  14
#   > 10      30 155   19      54  15
#   5-10       8  97    6       2   3

dim(t3)
# [1] 3 5 2

dimnames(t3)
# $Years
# [1] "< 5"  "> 10" "5-10"
#
# $Field
# [1] "beh_cog" "bio"     "chem"    "physics" "soc"
#
# $Status
# [1] "Not funded" "Funded"
